#pragma comment (lib, "Shlwapi.lib")
#include <stdio.h>
#include <windows.h>
#include <Winternl.h>
#include <Shlwapi.h>
#include <Shlobj.h>
#include <tchar.h>
#include <stdio.h>
#include <psapi.h>

#include <TlHelp32.h>
#define BEA_ENGINE_STATIC  /* specify the usage of a static version of BeaEngine */
#define BEA_USE_STDCALL    /* specify the usage of a stdcall version of BeaEngine */
#include "BeaEngine.h"

typedef struct  
{
	DWORD  dwSize;
	BYTE  szSectionName [8];
	LPVOID lpSectionStart;
	LPVOID lpSectionEnd;
} sectionData;

typedef struct  
{
	LPVOID moduleStart;
	LPVOID moduleEnd;
} NtdllData;

typedef struct 
{
	DWORD crc_func_name;
	DWORD crc_func_body;
} hashEntry;

typedef struct 
{
	DWORD  num_elements;
	hashEntry* ptr_hashdata;
} libData;

typedef struct  
{
	LPWSTR  str_library_path;
	libData* lpLibData;
} libListEntry;

typedef struct  
{
	DWORD  num_elements;
	libListEntry* lpList;
} libList;

typedef struct  
{
	PVOID			Reserved2[4];
	PVOID			imageBase;
	PVOID			imageEntryPoint;
	DWORD			imageSize;
	UNICODE_STRING	imageName;
} PEB_STRUCT;


/* Windows Internals */
LPVOID GetPebAddress(HANDLE hProc);
LPVOID GetPebLdrData (HANDLE hProc);
LPVOID GetExportDirectoryHandle (LPVOID lpMappedLib);
void RelocateModuleToAddress( LPVOID remoteMem, LPVOID relocTo );
LPVOID MapPELibrary (LPWSTR szLibName, LPVOID base, LPDWORD retSize);


/* Structures */
LPVOID AllocateLibList (void);
BOOL InsertLibListElement( libList*& lpLibList, LPWSTR szLibName, libData* lpLibData );
void enumLibList (libList* lpLibList);
BOOL IsLibraryInLibList (libList* lpLibList, LPWSTR szLibName);
BOOL FreeLibList (libList* lpLibList);
DWORD GetNumFuncHashOccurences (LPVOID lpLibData, DWORD funcNameHash);
DWORD GetFuncBodyHashByNameHash (libData* lpLibData, DWORD funcNameHash);


/* Helpers */
DWORD CalculateCRC32(const void *data, DWORD size);
LPWSTR GetSpecialPathByCSIDL (int csidl);
LPWSTR Get64BitSystemPath (void);
LPWSTR Get32BitSystemPath (void);
BOOL IsSystemLibraryPath (LPWSTR szPath);
DWORD GetPIDByProcessName(LPWSTR szName);
LPBYTE ReadFileToMem( LPWSTR fileName, LPDWORD bytesRead );
LPWSTR ReadUnicodeStrFromMemory (HANDLE hProc, UNICODE_STRING lpUnicode);
BOOL ReadPEHeadersFromProc(HANDLE hProc, LPVOID lpImageBase, PIMAGE_DOS_HEADER lpRetDos, PIMAGE_NT_HEADERS lpRetNT);
LPSTR ReadAnsiStrFromMemory (HANDLE hProc, LPVOID addr);
void RelocateModuleToAddress( LPVOID remoteMem, LPVOID relocTo );
BOOL AdjustTokenToDebug();

/* CORE */
DWORD CalculateCRCHash (LPBYTE funcData, DWORD funcLen, BOOL debugPrint = 0);
libData* GenerateHashTableForMappedLib (LPVOID lpMappedLib, DWORD debug = 0);
libData* ProcessPELibrary(LPWSTR szLibName, LPVOID szLibImageBase, libList* lplibList);
BOOL ReadPESectionsInfoFromProc (HANDLE hProc, LPVOID lpImageBase, sectionData* lpSectionData, NtdllData* lpNtData, BOOL isNtDLL);
void ProcessSingleModule (HANDLE hProc, LPWSTR szModulePath, LPVOID lpDestImageBase, libList* lpLibraryList, NtdllData*& lpNtData);
void ScanProcessByHandle (HANDLE hProc);
void ScanSingleProcess(DWORD dwPID);
void ScanAllProcesses(void);
BOOL CompareHashes(HANDLE hProc, LPVOID moduleImageBase, libData* lpLibData, sectionData* lpSectionData, NtdllData* lpNtdllData);
BOOL ShouldSkipTheLib(LPWSTR szLibPath);

