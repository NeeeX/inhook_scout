#pragma once
#include "internal.h"

LPWSTR libsToCheck [] = {
	L"ntdll.dll",
	L"kernel32.dll",
	L"kernelbase.dll",
	L"advapi32.dll",
	L"sechost.dll",
	L"rpcrt4.dll",
	L"gdi32.dll",
	L"user32.dll",
	L"shlwapi.dll",
	L"shell32.dll",
	L"ole32.dll",
	L"sspicli.dll",
	L"cryptbase.dll",
	L"wininet.dll",
	L"ws2_32.dll"
};


DWORD CalculateCRCHash (LPBYTE funcData, DWORD funcLen, BOOL debugPrint)
{
	DWORD dwTempHash = 0;

	dwTempHash = CalculateCRC32(funcData, funcLen);

	if (debugPrint) printf("Calculates hash = %X\n", dwTempHash);

	return dwTempHash;
}

BOOL ShouldSkipTheLib(LPWSTR szLibPath)
{
	if (!szLibPath) return FALSE;
	LPWSTR szLibFilename = PathFindFileNameW(szLibPath);
	if (!szLibFilename) return FALSE;

	for (unsigned int i = 0; i < sizeof(libsToCheck)/sizeof(LPWSTR); i++)
	{
		if (StrCmpW(szLibFilename, libsToCheck[i]) == 0) return FALSE;
	}

	return TRUE;
}


/*
Generates the hashes for functions contained in pe library allocates under lpMappedLib VA.
*/
libData* GenerateHashTableForMappedLib (LPVOID lpMappedLib, DWORD debug)
{
	DWORD dwNumFunctionsInLibrary = 0;
	libData* newLibData	   =(libData*)malloc(sizeof(libData));

	if (!newLibData) return NULL;

	PIMAGE_EXPORT_DIRECTORY lpExportDir = (PIMAGE_EXPORT_DIRECTORY)GetExportDirectoryHandle(lpMappedLib);

	if (!lpExportDir || lpExportDir->NumberOfNames <= 0) return NULL;

	dwNumFunctionsInLibrary	  = lpExportDir->NumberOfNames;

	hashEntry* newHashData = (hashEntry*)malloc(dwNumFunctionsInLibrary * sizeof(hashEntry));

	if (!newHashData) return FALSE;

	for (unsigned int i = 0; i < dwNumFunctionsInLibrary; i++)
	{
		LPDWORD funcNameVA = (LPDWORD)(((LPBYTE)lpMappedLib) + lpExportDir->AddressOfNames + (i * sizeof(DWORD)));

		if (funcNameVA)
		{
			LPWORD l1 = (LPWORD)(((LPBYTE)lpMappedLib) + lpExportDir->AddressOfNameOrdinals + (i * sizeof(WORD)));

			if (l1)
			{
				LPSTR funcName = (LPSTR)((LPBYTE)lpMappedLib + *funcNameVA);
				LPDWORD funcVA = (LPDWORD)(((LPBYTE)lpMappedLib) + lpExportDir->AddressOfFunctions + ((*l1) * sizeof(DWORD)));

	

				newHashData[i].crc_func_name = CalculateCRCHash((LPBYTE)funcName, strlen(funcName)); //Hash of function name
				newHashData[i].crc_func_body = CalculateCRCHash((LPBYTE)lpMappedLib + *funcVA, 5);   //Hash of function body
			}

		}
	}


	newLibData->num_elements = dwNumFunctionsInLibrary;
	newLibData->ptr_hashdata	 = newHashData;
	return newLibData;
}




/*
	Reads the library to memory. Performs PE mapping. Fixes the relocation table  for szLibImageBase address.
	Generates the hashes for functions contained in this PE library.
	Returns the address of the library "hash table"

*/
libData* ProcessPELibrary(LPWSTR szLibName, LPVOID szLibImageBase, libList* lplibList)
{
	DWORD dwRet			= 0;
	libData* lpLibData	= NULL;
	LPVOID mapAddr		= MapPELibrary(szLibName, (LPVOID)szLibImageBase, &dwRet);
	 
	if (mapAddr && dwRet > 0)
	{
		lpLibData  = GenerateHashTableForMappedLib(mapAddr);

		if (lpLibData)
		{
			VirtualFree(mapAddr, 0, MEM_RELEASE);

			//Insert the "hash table" generated for this library into global library list
			if (InsertLibListElement(lplibList, szLibName, lpLibData))
				return lpLibData;
		}
	}

	return NULL;
}


/*
	Reads layout of PE sections for given library residing under lpImageBase in process with hProc handle.
	The read information is then stored in lpSectionData array. The information read to array includes:

	- start VA of the section
	- end   VA of the section
	- ANSI section name

*/
BOOL ReadPESectionsInfoFromProc (HANDLE hProc, LPVOID lpImageBase, sectionData* lpSectionData, NtdllData* lpNtData, BOOL isNtDLL)
{
	unsigned int b = 0;
	IMAGE_DOS_HEADER headersDos;
	IMAGE_NT_HEADERS headersNT;

	if (!ReadPEHeadersFromProc(hProc, lpImageBase, &headersDos, &headersNT)) return FALSE;

	for (unsigned int i = 0; i < headersNT.FileHeader.NumberOfSections; i++)
	{
		IMAGE_SECTION_HEADER lpSection = {0};

		if (ReadProcessMemory(hProc, 
			(PIMAGE_SECTION_HEADER)(((LPBYTE)lpImageBase + headersDos.e_lfanew + sizeof(IMAGE_NT_HEADERS)) + (i * sizeof(IMAGE_SECTION_HEADER))),
			(LPVOID)&lpSection, sizeof(IMAGE_SECTION_HEADER),0 ))
		{
			lpSectionData[i].lpSectionStart		= (LPBYTE)lpImageBase+lpSection.VirtualAddress;
			lpSectionData[i].lpSectionEnd		= (LPBYTE)lpImageBase+lpSection.VirtualAddress+lpSection.Misc.VirtualSize;

			memcpy(lpSectionData[i].szSectionName, lpSection.Name, 8);

			if (isNtDLL && (lstrcmpA((LPSTR)lpSection.Name, ".text") == 0) )
			{
				lpNtData->moduleStart = (LPBYTE)lpImageBase+lpSection.VirtualAddress;
				lpNtData->moduleEnd   = (LPBYTE)lpImageBase+lpSection.VirtualAddress+lpSection.Misc.VirtualSize;
			}

			b++;
		}		
	}

	lpSectionData[0].dwSize = (b * sizeof(sectionData));

	return TRUE;
}


void ProcessSingleModule (HANDLE hProc, LPWSTR szModulePath, LPVOID lpDestImageBase, libList* lpLibraryList, NtdllData*& lpNtData)
{
	sectionData sectionsInformation [160]; //We won't have more than 5-10 sections anyway..
	libData* moduleLibData;

	ZeroMemory(sectionsInformation, sizeof(sectionsInformation));

	if ((moduleLibData = ProcessPELibrary(szModulePath, lpDestImageBase, lpLibraryList)) != NULL)
	{
		//Exceptional behaviour for ntdll.dll
		if (lstrcmpW(szModulePath, L"ntdll.dll") == 0)
			ReadPESectionsInfoFromProc(hProc, lpDestImageBase, sectionsInformation, lpNtData, TRUE);
		else
			ReadPESectionsInfoFromProc(hProc, lpDestImageBase, sectionsInformation, 0, FALSE);

		//compareHashesEx(hProc, lpDestImageBase, moduleLibData, sectionsInformation, lpNtData);
	}

};

void ScanSingleProcess(DWORD dwPID)
{
	if (!dwPID) return ;

	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, false, dwPID);

	if (hProc)
	{
		ScanProcessByHandle(hProc);
		CloseHandle(hProc);
	}
}

void ScanAllProcesses(void)
{
	DWORD  dwResult  = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (!hSnapshot || hSnapshot == INVALID_HANDLE_VALUE) return ;

	PROCESSENTRY32W pe32;
	pe32.dwSize = sizeof(pe32);

	if (!Process32FirstW(hSnapshot, &pe32)) return ;

	do
	{
		HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, false, pe32.th32ProcessID);

		if (hProc != INVALID_HANDLE_VALUE)
		{
			ScanProcessByHandle(hProc);
			CloseHandle(hProc);
		}	
	} while (Process32Next(hSnapshot, &pe32) && pe32.th32ProcessID);

	CloseHandle(hSnapshot);
}

BOOL AddressBelongsToRegisteredModules(HANDLE hProcess, LPVOID address)
{
	HMODULE hMods[1024];
	DWORD cbNeeded;
	unsigned int i;


	// Get a list of all the modules in this process.

	if( EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
	{

		for ( i = 0; i < (cbNeeded / sizeof(HMODULE)); i++ )
		{
			TCHAR szModName[MAX_PATH];

			// Get the full path to the module's file.

			if ( GetModuleFileNameEx( hProcess, hMods[i], szModName,
				sizeof(szModName) / sizeof(TCHAR)))
			{
				MODULEINFO module;

				GetModuleInformation(hProcess, hMods[i], &module, sizeof(module));
				// Print the module name and handle value.

				LPBYTE moduleStart = (LPBYTE)module.lpBaseOfDll;
				LPBYTE moduleEnd   = (LPBYTE)module.lpBaseOfDll + module.SizeOfImage;

				if (address >= moduleStart && address <= moduleEnd)
				{
					return TRUE;
				}
			} 
		}
	}

	return FALSE;
}


BOOL CompareHashes(HANDLE hProc, LPVOID moduleImageBase, libData* lpLibData, sectionData* lpSectionData, NtdllData* lpNtdllData)
{
	DWORD dwExportElements				= 0;
	DWORD dwFuncDataHash;
	DWORD dwFuncNameHash;
	DWORD dwOrginalHash;
	BOOL bRet							= FALSE;
	LPBYTE exportBuffer					= NULL;
	IMAGE_DOS_HEADER headersDos;
	IMAGE_NT_HEADERS headersNT;
	PIMAGE_EXPORT_DIRECTORY lpExportDir = NULL;

	LPBYTE lpExportTmp;

	if (!ReadPEHeadersFromProc(hProc, moduleImageBase, &headersDos, &headersNT)) return FALSE;

	PIMAGE_DATA_DIRECTORY	 lpDataDir	 = &headersNT.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];

	lpExportTmp = (LPBYTE)malloc(lpDataDir->Size);

	if (ReadProcessMemory(hProc, (LPBYTE)moduleImageBase + lpDataDir->VirtualAddress, lpExportTmp,  lpDataDir->Size,0 ))
	{
		lpExportDir = (PIMAGE_EXPORT_DIRECTORY)lpExportTmp;
	}

	if (!lpExportDir || lpExportDir->NumberOfNames <= 0) 
	{
		free(lpExportTmp);
		return FALSE;
	}

	dwExportElements	  = lpExportDir->NumberOfNames;

	for (unsigned int i = 0; i < dwExportElements; i++)
	{
		DWORD funcNameVA = 0;
		DWORD funcVA	= 0;
		WORD l1			= 0;

		if (!ReadProcessMemory(
			hProc,
			(((LPBYTE)moduleImageBase) + lpExportDir->AddressOfNames + (i * sizeof(DWORD))),
			(LPVOID)&funcNameVA,
			sizeof(funcNameVA),
			0 ) ||
			!funcNameVA)
		{
			continue;
		}

		if (!ReadProcessMemory(
			hProc,
			(((LPBYTE)moduleImageBase) + lpExportDir->AddressOfNameOrdinals + (i * sizeof(WORD))),
			(LPVOID)&l1,
			sizeof(l1),
			0 ) || !l1)
		{
			continue;
		}

		if (!ReadProcessMemory(
			hProc,
			(((LPBYTE)moduleImageBase) + lpExportDir->AddressOfFunctions + ((l1) * sizeof(DWORD))),
			(LPVOID)&funcVA,
			sizeof(funcVA),
			0 ))
		{
			continue;
		}

		BYTE lpFuncData [20] = {0};
		LPSTR szFuncName	= ReadAnsiStrFromMemory(hProc, (LPBYTE)moduleImageBase + funcNameVA);

		if (!szFuncName ||! ReadProcessMemory(hProc, (LPBYTE)moduleImageBase + funcVA, lpFuncData,  sizeof(lpFuncData),0 ))
		{
			continue;
		}

		bRet = FALSE;
		dwFuncNameHash =  CalculateCRCHash((LPBYTE)szFuncName, strlen(szFuncName));
		dwFuncDataHash =  CalculateCRCHash(lpFuncData, 5);
		dwOrginalHash  =  GetFuncBodyHashByNameHash(lpLibData, dwFuncNameHash);




		if (dwOrginalHash != dwFuncDataHash)
		{
			for (unsigned int i = 0; i < (lpSectionData[0].dwSize/sizeof(sectionData)); i++)
			{
				if ((LPBYTE)moduleImageBase + funcVA >= lpSectionData[i].lpSectionStart && (LPBYTE)moduleImageBase + funcVA <= lpSectionData[i].lpSectionEnd )
				{
					if (lstrcmpA((LPSTR)lpSectionData[i].szSectionName, ".data") != 0)
					{
						


						DISASM MyDisasm;
						int len;
						int Error = 0;

						/* ============================= Init the Disasm structure (important !)*/
						(void) memset (&MyDisasm, 0, sizeof(DISASM));

						/* ============================= Init EIP */
						MyDisasm.EIP = (UIntPtr)lpFuncData;
					

						/* ============================= Loop for Disasm */
						while (!Error){
							/* ============================= Fix SecurityBlock */
							MyDisasm.SecurityBlock = (UIntPtr)sizeof(lpFuncData);

							len = Disasm(&MyDisasm);
							if (len == OUT_OF_BLOCK) {
								Error = 1;
							}
							else if (len == UNKNOWN_OPCODE) {
								Error = 1;
							}
							else {
								if (MyDisasm.Instruction.Opcode == 0xC2 || MyDisasm.Instruction.Opcode == 0xC3) break;
								else if (MyDisasm.EIP >= (UIntPtr)lpFuncData + sizeof(lpFuncData) -1) break;
								else if (MyDisasm.Instruction.BranchType == JmpType && MyDisasm.Instruction.AddrValue != 0)
								{
									
									BOOL bBelongs = AddressBelongsToRegisteredModules(hProc, (LPVOID)MyDisasm.Instruction.AddrValue);

									printf("[!] %s Hash = %X Original Hash = %X\n", szFuncName, dwFuncNameHash, dwFuncDataHash, dwOrginalHash);
									printf("[!] Jump to %X (%d) found\n", MyDisasm.Instruction.AddrValue, (bBelongs ? 1 : 0));

									break;
								}
								MyDisasm.EIP = MyDisasm.EIP + (UIntPtr)len;
							}
						};

						/*
						printf("[M] %s (%X) %s (libTable=%X || Cur=%X) libTableOccrs=%d VA=%X RVA=%X\n ",
							szFuncName,
							dwFuncNameHash,
							lpSectionData[i].szSectionName,
							dwFuncDataHash,
							dwOrginalHash,
							GetNumFuncHashOccurences(lpLibData, dwFuncNameHash),
							funcVA,
							(LPBYTE)moduleImageBase + funcVA);
						*/
					}

					bRet = TRUE;
				}
			}

			if (bRet == FALSE)
			{
				if (!((LPBYTE)moduleImageBase + funcVA >= lpNtdllData->moduleStart && (LPBYTE)moduleImageBase + funcVA <= lpNtdllData->moduleEnd))
				{
					//printf("Did not find section name %s %X \n", szFuncName, lpFuncData);
				}
			}
		}

		free(szFuncName);		
	}

	free(lpExportTmp);
	return TRUE;
}


void ScanProcessByHandle (HANDLE hProc)
{
	LPVOID moduleFlink;
	LPVOID moduleTmp;
	LPWSTR modulePath;
	NtdllData lpNtData;

	_PEB_LDR_DATA* buff_ldr = (_PEB_LDR_DATA*) GetPebLdrData(hProc);

	if (!buff_ldr) return ;

	libList* appLibList = (libList*)AllocateLibList();

	if (!appLibList) {
		free(buff_ldr);
		return ;
	}

	moduleFlink = buff_ldr->InMemoryOrderModuleList.Flink;
	moduleTmp  = moduleFlink;


	//Enumerate module by module
	do 
	{	
		sectionData sectionsInformation [160];
		PEB_STRUCT loaderData;
		libData* moduleLibData = NULL;

		if (!ReadProcessMemory(hProc, (LPVOID)((DWORD)moduleTmp), &loaderData, sizeof(PEB_STRUCT), 0))
		{
			printf("Cannot read module information");
			ReadProcessMemory(hProc, (LPVOID)moduleTmp, &moduleTmp, sizeof(LPVOID), 0);
			continue;
		}

		if (!loaderData.imageBase || loaderData.imageSize <= 0)
		{
			ReadProcessMemory(hProc, (LPVOID)moduleTmp, &moduleTmp, sizeof(LPVOID), 0);
			continue;
		}

		modulePath			  =  CharLowerW(ReadUnicodeStrFromMemory(hProc, loaderData.imageName));

		LPWSTR moduleFileName =  PathFindFileName(modulePath);

		if (loaderData.imageEntryPoint <= 0 && lstrcmpW(moduleFileName, L"ntdll.dll") != 0) 
		{
			ReadProcessMemory(hProc, (LPVOID)moduleTmp, &moduleTmp, sizeof(LPVOID), 0);
			continue;
		}

		if (ShouldSkipTheLib(modulePath))
		{
			ReadProcessMemory(hProc, (LPVOID)moduleTmp, &moduleTmp, sizeof(LPVOID), 0);
			continue;
		}

		if (!modulePath || !IsSystemLibraryPath(modulePath))
		{
			ReadProcessMemory(hProc, (LPVOID)moduleTmp, &moduleTmp, sizeof(LPVOID), 0);
			continue;
		}

		wprintf(L"==============================================\n");
		wprintf(L"Module:\t\t%s\nImagebase:\t%X\nEntrypoint:\t%X\nImagesize:\t%d\n", modulePath, loaderData.imageBase, loaderData.imageEntryPoint, loaderData.imageSize);
		wprintf(L"==============================================\n");

		if ((moduleLibData = ProcessPELibrary(modulePath, loaderData.imageBase, appLibList)) != NULL)
		{
			//Exceptional behaviour for ntdll.dll
			if (lstrcmpW(moduleFileName, L"ntdll.dll") == 0)
				ReadPESectionsInfoFromProc(hProc, loaderData.imageBase, sectionsInformation, &lpNtData, TRUE);
			else
				ReadPESectionsInfoFromProc(hProc, loaderData.imageBase, sectionsInformation, 0, FALSE);

			CompareHashes(hProc, loaderData.imageBase, moduleLibData, sectionsInformation, &lpNtData);
		}

		free(modulePath);

		ReadProcessMemory(hProc, (LPVOID)moduleTmp, &moduleTmp, sizeof(LPVOID), 0);
	} while (moduleTmp != NULL && moduleTmp != moduleFlink);

	FreeLibList(appLibList);
	free(buff_ldr);
}
