#include "internal.h"
#pragma comment(lib, "psapi")


int main(int argc, LPSTR argv[])
{

	if (argc != 2) {
		printf("Usage app.exe <PID>");
		return 1;
	}

	DWORD dwTargetPID = atoi(argv[1]); 

	//Enable SE_DEBUG for >= XP
	AdjustTokenToDebug();
	
	//User supplied us with PID
	if (dwTargetPID) ScanSingleProcess(dwTargetPID); //core.cpp

	return 0;
}

