#pragma once
#include "internal.h"


/* PEB */

LPVOID GetPebAddress( HANDLE hProc )
{
	typedef NTSTATUS (WINAPI* sNtQueryInformationProcess)(HANDLE, PROCESSINFOCLASS, PVOID, ULONG, PULONG); //Not documented

	DWORD num_retsize;
	PROCESS_BASIC_INFORMATION buff_pbi;
	sNtQueryInformationProcess fNtQueryInformationProcess;

	fNtQueryInformationProcess = (sNtQueryInformationProcess)GetProcAddress(LoadLibrary(L"ntdll.dll"), "NtQueryInformationProcess");

	if (fNtQueryInformationProcess != NULL)
	{
		DWORD dwHey = fNtQueryInformationProcess(hProc, ProcessBasicInformation, &buff_pbi, sizeof(buff_pbi), &num_retsize);

		if (num_retsize == sizeof(PROCESS_BASIC_INFORMATION))
			return buff_pbi.PebBaseAddress;
	}

	return NULL;
}

LPVOID GetPebLdrData( HANDLE hProc )
{
	LPVOID pebAddr;
	_PEB   pebData;
	DWORD  dwBytesRead;
	_PEB_LDR_DATA* pebLdrData = (_PEB_LDR_DATA*)malloc(sizeof(_PEB_LDR_DATA));

	pebAddr = GetPebAddress(hProc);

	if (pebAddr)
	{
		if (ReadProcessMemory(hProc, pebAddr, &pebData, sizeof(_PEB), &dwBytesRead) && pebData.Ldr)
		{
			if (ReadProcessMemory(hProc, pebData.Ldr, pebLdrData, sizeof(_PEB_LDR_DATA), &dwBytesRead))
				return pebLdrData;
		}
	}

	return NULL;
}

/* Parses PE headers and extracts VA to export directory holding function names */
LPVOID GetExportDirectoryHandle (LPVOID lpMappedLib)
{
	PIMAGE_DOS_HEADER lpDos = (PIMAGE_DOS_HEADER)lpMappedLib;

	if (lpDos->e_magic == IMAGE_DOS_SIGNATURE)
	{
		PIMAGE_NT_HEADERS lpNt = (PIMAGE_NT_HEADERS)((LPBYTE)lpMappedLib + lpDos->e_lfanew);

		if (lpNt->Signature == IMAGE_NT_SIGNATURE)
		{
			PIMAGE_DATA_DIRECTORY	 lpDataDir	 = &lpNt->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
			PIMAGE_EXPORT_DIRECTORY  lpExportDir = (PIMAGE_EXPORT_DIRECTORY)((LPBYTE)lpMappedLib + lpDataDir->VirtualAddress);

			if (lpExportDir)
				return lpExportDir;
		}
	}

	return NULL;
}


/* Walks through relocation table and fixes the offsets to match new imagebase */
void RelocateModuleToAddress( LPVOID remoteMem, LPVOID relocTo )
{
	PIMAGE_NT_HEADERS ntHeader = (PIMAGE_NT_HEADERS)((LPBYTE)remoteMem + ((PIMAGE_DOS_HEADER)remoteMem)->e_lfanew);

	if (ntHeader->Signature == IMAGE_NT_SIGNATURE)
	{
		IMAGE_DATA_DIRECTORY *relocsDir = &ntHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC];

		if(relocsDir->Size > 0 && relocsDir->VirtualAddress > 0)
		{
			DWORD_PTR delta               = (DWORD_PTR)((LPBYTE)relocTo - ntHeader->OptionalHeader.ImageBase);
			IMAGE_BASE_RELOCATION *relHdr = (IMAGE_BASE_RELOCATION *)((LPBYTE)remoteMem + relocsDir->VirtualAddress);

			while(relHdr->VirtualAddress != 0)
			{
				if(relHdr->SizeOfBlock >= sizeof(IMAGE_BASE_RELOCATION))
				{
					DWORD relCount = (relHdr->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD);
					LPWORD relList = (LPWORD)((LPBYTE)relHdr + sizeof(IMAGE_BASE_RELOCATION));

					for(DWORD i = 0; i < relCount; i++)if(relList[i] > 0)
					{

						DWORD_PTR *p = (DWORD_PTR *)((LPBYTE)remoteMem + (relHdr->VirtualAddress + (0x0FFF & (relList[i]))));

						*p += delta;
					}
				}

				relHdr = (IMAGE_BASE_RELOCATION *)((LPBYTE)relHdr + relHdr->SizeOfBlock);
			}
		}

	}
}


/* Performs the MANUAL mapping of PE library This includes fixing offsets in relocation table */
LPVOID MapPELibrary (LPWSTR szLibName, LPVOID destImageBase, LPDWORD retSize)
{
	DWORD dwRead		= 0;
	LPVOID fileMapAddr  = ReadFileToMem(szLibName, &dwRead);
	LPVOID imageMapAddr = NULL;

	if (fileMapAddr)
	{
		PIMAGE_DOS_HEADER lpDos = (PIMAGE_DOS_HEADER)fileMapAddr;

		if (lpDos->e_magic == IMAGE_DOS_SIGNATURE)
		{		
			PIMAGE_NT_HEADERS lpNt = (PIMAGE_NT_HEADERS)((LPBYTE)fileMapAddr + lpDos->e_lfanew);
			if (lpNt->Signature == IMAGE_NT_SIGNATURE)
			{
				imageMapAddr = VirtualAlloc(0, lpNt->OptionalHeader.SizeOfImage, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

				if (imageMapAddr)
				{
					*retSize = lpNt->OptionalHeader.SizeOfImage;
					memcpy(imageMapAddr, fileMapAddr, lpNt->OptionalHeader.SizeOfHeaders);

					for (unsigned int i = 0; i < lpNt->FileHeader.NumberOfSections; i++)
					{
						PIMAGE_SECTION_HEADER lpSection = (PIMAGE_SECTION_HEADER)((LPBYTE)fileMapAddr + lpDos->e_lfanew + sizeof(IMAGE_NT_HEADERS) + (i * sizeof(IMAGE_SECTION_HEADER)));

						if (lpSection)
							memcpy(((LPBYTE)imageMapAddr+lpSection->VirtualAddress), ((LPBYTE)fileMapAddr+lpSection->PointerToRawData), lpSection->SizeOfRawData);
					}

					RelocateModuleToAddress(imageMapAddr, destImageBase);
				}
			}

		}

		VirtualFree(fileMapAddr, 0, MEM_RELEASE );

	}

	return imageMapAddr;
}
