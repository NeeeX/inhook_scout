#pragma once
#include "internal.h"

/* Allocate and initalize new libList structure */
LPVOID AllocateLibList( void )
{
	libList* libListPtr = (libList*)malloc(sizeof(libList));

	if (libListPtr)
	{
		libListPtr->num_elements = 0;
		libListPtr->lpList   = NULL;

		return libListPtr;
	}
	else
		return NULL;
}



/* Append new element at the end of libList structure
   Usually used when we map a new system library
*/
BOOL InsertLibListElement( libList*& lpLibList, LPWSTR szLibName, libData* lpLibData )
{
	DWORD dwCounter = 0;

	if (lpLibList->lpList == NULL)
		lpLibList->lpList = (libListEntry*)malloc(sizeof(libListEntry));
	else
		lpLibList->lpList = (libListEntry*)realloc(lpLibList->lpList, ((lpLibList->num_elements+1) * sizeof(libListEntry)));

	if (lpLibList->lpList)
	{
		libListEntry* libListEntryPtr = (libListEntry*)lpLibList->lpList;

		if (libListEntryPtr)
		{
			libListEntryPtr[(lpLibList->num_elements)].str_library_path  = szLibName;
			libListEntryPtr[(lpLibList->num_elements)].lpLibData  = lpLibData;
			lpLibList->num_elements								  = lpLibList->num_elements + 1;

			return TRUE;
		}
	}

	return FALSE;
}

/* Checks whether library with given name has been loaded (Is in liblist structure) */
BOOL IsLibraryInLibList( libList* lpLibList, LPWSTR szLibName )
{
	libListEntry* libListEntryPtr = (libListEntry*)lpLibList->lpList;

	if (libListEntryPtr)
	{
		for (unsigned int i = 0; i < lpLibList->num_elements; i++)
		{
			if (lstrcmpW(szLibName, libListEntryPtr[i].str_library_path) == 0)
				return TRUE;
		}
	}

	return FALSE;
}

/* Enumerates libList. Walks libData and deallocates mapped libraries */
BOOL FreeLibList( libList* lpLibList )
{
	libListEntry* libListEntryPtr = (libListEntry*)lpLibList->lpList;

	if (libListEntryPtr)
	{
		for (unsigned int i = 0; i < lpLibList->num_elements; i++)
		{
			libData* lpLibData = (libData*)libListEntryPtr[i].lpLibData;

			if (lpLibData)
			{
				free(lpLibData->ptr_hashdata);
				free(libListEntryPtr[i].lpLibData);
				libListEntryPtr[i].str_library_path = NULL;
				libListEntryPtr[i].lpLibData = NULL;
			}
		}
	}

	return FALSE;
}


/* Debug only */
DWORD GetNumFuncHashOccurences( LPVOID lpLibData, DWORD funcNameHash )
{
	DWORD g = 0;
	libData* libDataPtr = (libData*)lpLibData;

	if (libDataPtr->num_elements > 0 && libDataPtr->ptr_hashdata)
	{
		hashEntry* hashDataPtr = (hashEntry*)libDataPtr->ptr_hashdata;

		if (hashDataPtr)
		{
			for (unsigned int i = 0; i < libDataPtr->num_elements; i++)
			{
				if (hashDataPtr[i].crc_func_name == funcNameHash)
					g++;
			}
		}
	}

	return g;
}

/* Gets the hash of first x bytes of function for given function name hash
   Search by hash is performance wise against search by regular function string.
*/
DWORD GetFuncBodyHashByNameHash( libData* lpLibData, DWORD funcNameHash )
{
	libData* libDataPtr = lpLibData;

	if (libDataPtr->num_elements > 0 && libDataPtr->ptr_hashdata)
	{
		hashEntry* hashDataPtr = (hashEntry*)libDataPtr->ptr_hashdata;

		if (hashDataPtr)
		{
			for (unsigned int i = 0; i < libDataPtr->num_elements; i++)
			{
				if (hashDataPtr[i].crc_func_name == funcNameHash)
					return hashDataPtr[i].crc_func_body;
			}

		}
	}

	return 0;
}
