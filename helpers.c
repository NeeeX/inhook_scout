#pragma once
#include "internal.h"

bool crc32Intalized = false;
static DWORD crc32table[256];

/* Could have used cryptoapi... implementation taken from the net */
DWORD CalculateCRC32(const void *data, DWORD size)
{
	if(crc32Intalized == false)
	{
		register DWORD crc;
		for(register DWORD i = 0; i < 256; i++)
		{
			crc = i;
			for(register DWORD j = 8; j > 0; j--)
			{
				if(crc & 0x1)crc = (crc >> 1) ^ 0xEDB88320L;
				else crc >>= 1;
			}
			crc32table[i] = crc;
		}

		crc32Intalized = true;
	}

	register DWORD cc = 0xFFFFFFFF;
	for(register DWORD i = 0; i < size; i++)cc = (cc >> 8) ^ crc32table[(((LPBYTE)data)[i] ^ cc) & 0xFF];
	return ~cc;
}


/* Useful for getting paths to special folders like Windows/APPDATA/Userprofile etc... */
LPWSTR GetSpecialPathByCSIDL (int csidl)
{
	LPWSTR szBuffer = (LPWSTR)malloc(MAX_PATH * sizeof(WCHAR));

	if (szBuffer)
	{
		if (SUCCEEDED(SHGetFolderPath(0, csidl, 0, SHGFP_TYPE_DEFAULT, szBuffer)))
			return szBuffer;
		else
			free(szBuffer);
	}

	return NULL;
}

/* Gets path to a system folder where 64bit libraries are stored */
LPWSTR Get64BitSystemPath (void)
{
	return GetSpecialPathByCSIDL(CSIDL_SYSTEMX86);
}

/* Gets path to a system folder where 32bit libraries are stored */
LPWSTR Get32BitSystemPath (void)
{
	return GetSpecialPathByCSIDL(CSIDL_SYSTEM);
}

/* For given szPath library path, the function makes sure that loaded library is SYSTEM library.
   This is useful for making sure we don't check libraries loaded by 3rd party application vendors
*/
BOOL IsSystemLibraryPath (LPWSTR szPath)
{
	LPWSTR sz32BitPath;
	LPWSTR sz64BitPath;
	LPWSTR szPathFileName;
	LPWSTR szPathExtension;
	BOOL bRet = FALSE;

	sz32BitPath = Get32BitSystemPath();
	sz64BitPath = Get64BitSystemPath();

	szPathFileName  = PathFindFileName(szPath);
	szPathExtension = PathFindExtension(szPath);

	if (sz32BitPath && sz64BitPath && szPathFileName)
	{
		if (PathAppend(sz32BitPath, szPathFileName) && PathAppend(sz64BitPath, szPathFileName))
		{
			if ((PathMatchSpec(sz32BitPath, szPath) || PathMatchSpec(sz64BitPath, szPath)) && (lstrcmpW(szPathExtension, L".dll") == 0))
			{
				bRet = TRUE;
			}
		}

		free(sz32BitPath);
		free(sz64BitPath);
	}

	return bRet;
}

/* Get process ID by process name. Returns the first match. */
DWORD GetPIDByProcessName(LPWSTR szName)
{
	DWORD  dwResult  = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (hSnapshot && hSnapshot != INVALID_HANDLE_VALUE)
	{
		PROCESSENTRY32W pe32;
		pe32.dwSize = sizeof(pe32);

		if (Process32FirstW(hSnapshot, &pe32) == TRUE)
		{
			do
			{
				if (StrCmpW(szName, pe32.szExeFile) == 0)
				{
					dwResult = pe32.th32ProcessID;
					break;
				}
			}
			while (Process32Next(hSnapshot, &pe32));

		}
	}

	CloseHandle(hSnapshot);

	return dwResult;
}

/* Reads a file to memory. Marks the memory page as RWE.
   ! The entire file is being read.
*/
LPBYTE ReadFileToMem( LPWSTR fileName, LPDWORD bytesRead )
{
	DWORD  fileSize = 0;
	HANDLE hFile = CreateFile(fileName, GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);

	if (hFile && hFile != INVALID_HANDLE_VALUE)
	{
		fileSize = GetFileSize(hFile, 0);
		LPBYTE fileBuffer = (LPBYTE)VirtualAlloc(0, fileSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

		if (fileBuffer)
		{		
			if (ReadFile(hFile, fileBuffer, fileSize, bytesRead, 0))
			{
				CloseHandle(hFile);
				return fileBuffer;
			}
		}
	}

	CloseHandle(hFile);
	return 0;
}

/* Used with windows internal functions for UNICODE_STRING structure */
LPWSTR ReadUnicodeStrFromMemory (HANDLE hProc, UNICODE_STRING lpUnicode)
{
	LPWSTR szBuffer = (LPWSTR)malloc(lpUnicode.Length * sizeof(WCHAR));

	if (szBuffer)
	{
		if (ReadProcessMemory(hProc, lpUnicode.Buffer, szBuffer, lpUnicode.Length * sizeof(WCHAR), 0))
			return szBuffer;
		else
			free(szBuffer);
	}

	return NULL;
}


/* Reads ANSI string with unknown length and size from external process memory */
LPSTR ReadAnsiStrFromMemory (HANDLE hProc, LPVOID addr)
{
	DWORD dwLen = 0;
	char szTmp;

	do 
	{
		if (ReadProcessMemory(hProc, (LPBYTE)addr+dwLen, &szTmp, 1, 0))
		{
			if (szTmp != 0x00)
				dwLen++;
			else
				break;
		}
		else
			break;

	} while (1);

	if (dwLen)
	{
		LPSTR szBuffer = (LPSTR)malloc(dwLen+1);

		if (szBuffer)
		{
			if (ReadProcessMemory(hProc, (LPBYTE)addr, szBuffer, dwLen+1, 0))
				return szBuffer;
			else
				free(szBuffer);
		}
	}

	return NULL;
}


BOOL ReadPEHeadersFromProc(HANDLE hProc, LPVOID lpImageBase, PIMAGE_DOS_HEADER lpRetDos, PIMAGE_NT_HEADERS lpRetNT)
{
	if (!lpRetDos || !lpRetNT) return FALSE;

	if (!ReadProcessMemory(hProc, lpImageBase, (LPVOID)lpRetDos, sizeof(IMAGE_DOS_HEADER), 0)) return FALSE;
	if (lpRetDos->e_magic != IMAGE_DOS_SIGNATURE) return FALSE;
	if (!ReadProcessMemory(hProc, ((LPBYTE)lpImageBase + lpRetDos->e_lfanew), (LPVOID)lpRetNT, sizeof(IMAGE_NT_HEADERS),0 )) return FALSE;
	if (lpRetNT->Signature != IMAGE_NT_SIGNATURE) return FALSE;

	return TRUE;

}
/* Prints a szMessage. Appends GetLastError() error (if any present)s and string representation of GLE */
void PrintMessageWithErrorStr (LPWSTR szMessage)
{
	LPWSTR messageBuffer = NULL;
	DWORD dwSystemError = GetLastError();

	if (dwSystemError > 0)
	{
		DWORD dwRetSize = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 0, dwSystemError, 0, (LPWSTR)&messageBuffer, 0,  0);

		if (dwRetSize > 0)
			wprintf(L"%s.\nError code = %d\n Message: %s\n", szMessage, dwSystemError, messageBuffer);
	}	
}


BOOL AdjustTokenToDebug() 
{ 

	//Define variables. 
	LUID tLUID; HANDLE hToken; TOKEN_PRIVILEGES tTP, tTPOld; 
	DWORD lengthReturned; BOOL ret = TRUE; 

	//Fill the tLUID struct with our privilage info. 
	if(LookupPrivilegeValue(NULL,SE_DEBUG_NAME,&tLUID)) { 
		//Open the token up. 
		if(OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,&hToken)) { 
			//Modify it so we become teh debuggzors. 
			tTP.PrivilegeCount=1; 
			tTP.Privileges->Attributes=SE_PRIVILEGE_ENABLED; 
			tTP.Privileges->Luid.HighPart=tLUID.HighPart; 
			tTP.Privileges->Luid.LowPart=tLUID.LowPart; 
			//Make the changes and check for errors. 
			if(!AdjustTokenPrivileges(hToken,0,&tTP,sizeof(tTP),&tTPOld,&lengthReturned)) 
				ret = FALSE; //Bad 
			CloseHandle(hToken); 
		} else ret = FALSE; //Bad 
	} else ret = FALSE; //Bad 
	return ret; 
}