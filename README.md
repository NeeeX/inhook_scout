## InHOOKScout ##

Aplikacja sprawdza integralność implementacji funkcji wywodzących sie z bibliotek systemowych takich jak np kernel32.dll, user32.dll.

Podczas swojego działania aplikacja analizuje wskazany proces pod kątem modyfikacji funkcji systemowych przez
np. złośliwe oprogramowanie. Wspomniane najbardziej zaawansowane złośliwe oprogramowanie takie jak np. [ZeuS](http://en.wikipedia.org/wiki/Zeus_(Trojan_horse)), SpyEye nie działa na zasadzie zwykłego przechwytywania klawiszy poprzez hook klawiatury.
Zamiast tego, malware modyfikuje funkcje WINAPI w sposób niewidocznych dla programisty/uzytkownika. Najczesciej do takiej modyfikacji
dochodzi w procesach przeglądarek, np. iexplore.exe. W identyczny sposób działają rootkity, modyfikując funkcje odpowiedzialne za np. listowanie procesów.

Przykładowo przeglądarka Internet Explorer wykorzystuje wywołanie funkcji WININET - InternetSendRequest 
aby przesyłać dane z formularzy - np formularzy logowania do serwera zawierających dane takie jak login czy hasło.

Złośliwe oprogramowanie podmienia w procesie funkcje InternetSendRequest na własna, która działa w sposób identyczny do pierwotnej z tą
różnicą iż PRZECHWYTUJE dane które są wysyłane. Nie ma znaczenia czy serwer korzysta z zabezpieczenia w warstwie transportu takich jak SSL/TLS.
Dane są przechwytywane przed ich zaszyfrowaniem i wysłaniem np z użyciem SSL.

To tak jakby ktoś podmienił nam fizycznie klawiaturę na wizualnie identyczną z tą różnicą że ta nowa wysyła zapisane klawisze
gdzieś dalej ;)

## Zasada działania ##

1. Aplikacja odczytuje listę załadowanych bibliotek systemowych w procesie podanym przez użytkownika.
1. Następnie dla wszystkich funkcji zawartych w bibliotekach ZAŁADOWANYCH w procesie podanym przez użytkownika oblicza sumy kontrolne CRC dla pierwszych 5 bajtów implementacji funkcji.
1. Aplikacja w sposób manualny mapuje biblioteki systemowe do swojej pamieć (te które zostały znalezione w kroku 1) - zapobiegając tym samym modyfikacji przez malware (brak wywołań LoadLibrary itp)
1. Następnie dla wszystkich funkcji zawartej w zamapowanych bibliotekach z kroku 3 oblicza sumę kontrolna CRC dla pierwszych 5 bajtów implementacji funkcji.
1. Następuje porównanie sum kontrolnych CRC. Jeżeli sumy kontrolne się nie zgadzają można założyć ze doszło do modyfikacji funkcji w pamięci procesu ze strony złośliwego oprogramowania.

## Wynik analizy procesu przeglądarki ##

![b.jpg](https://bitbucket.org/repo/dAnRrx/images/3629075994-b.jpg)

## Wynik analizy procesu przeglądarki po infekcji trojanem bankowym [ZeuS](http://en.wikipedia.org/wiki/Zeus_(Trojan_horse)) ##

![a.jpg](https://bitbucket.org/repo/dAnRrx/images/370783438-a.jpg)